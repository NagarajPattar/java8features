package com.myzee.test;

public class ExtractClassDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}

// refer eclipse tips for extract class from existing. or refer extract class video to understand.
class Employee {
	int id;
	String name;
	EmployeeAddress data = new EmployeeAddress();
	
	public Employee(int id, String name, EmployeeAddress data) {
		super();
		this.id = id;
		this.name = name;
		this.data = data;
	}
	
}