package com.myzee.test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Clock;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class DateAndTime {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		Clock clock = Clock.systemDefaultZone();
		System.out.println(clock);                      //SystemClock[Asia/Calcutta]
		System.out.println(clock.instant().toString()); //2013-05-15T06:36:33.837Z
		System.out.println(clock.getZone());            //Asia/Calcutta
		System.out.println(Clock.systemUTC().millis());
		Files.list(Paths.get("..\\MyFirstAngularApp")).forEach(System.out::println);;
		Files.list(Paths.get("..\\..\\..\\..\\..")).forEach(System.out::println);;
		Files.list(Paths.get("C:\\Users")).forEach(System.out::println);;
		
		String anotherDate = "04 Apr 2016";
		 
		DateTimeFormatter df = DateTimeFormatter.ofPattern("dd MMM yyyy");
		LocalDate random = LocalDate.parse(anotherDate, df);
		 
		System.out.println(anotherDate + " parses as " + random);
	}
}