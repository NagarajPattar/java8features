package com.myzee.test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class StreamDotGenerate 
{
     public static void main(String[] args)
     {
         Stream<Date> stream = Stream.generate(() -> {return new Date();});
//         stream.forEach(p -> System.out.println(p.getSeconds()));
//         stream.forEach(s -> System.out.println(s));
         
         List<Integer> list = Arrays.asList(8, 4, 5, 9, 3, 2);
         list.stream().filter(p -> p%2==0).forEach(s -> System.out.println(s));
         
     }
}