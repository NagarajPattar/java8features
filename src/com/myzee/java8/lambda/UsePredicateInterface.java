package com.myzee.java8.lambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;

public class UsePredicateInterface {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Predicate<String> p = (str) -> str.contains("a");
		
		List<String> names = new ArrayList<String>(Arrays.asList("Bob", "alice", "ButhaN", "Phuentshooling", "border"));
		Iterator<String> itr = names.iterator();
		while(itr.hasNext()) {
			String s = itr.next();
			if(p.test(s)) {
				System.out.println("found in - " + s);
			}
		}
		System.out.println("testingggg");		
		p = (str) -> str.startsWith("N");
		System.out.println(p.test("alice"));
	}
}
