package com.myzee.java8.foreach;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public class FuncInterfaceWithForEach {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Ani> anlist = new ArrayList<Ani>();
		anlist.add(new Ani(9, "ali"));
		anlist.add(new Ani(711, "katherine"));
		anlist.add(new Ani(11, "bob"));
		/*
		 * <html>using java8 new 
		 * 'java.lang.Iterable.forEach(Consumer<? super Ani> action)' feature
		 */
		System.out.println("<html>using java8 new "
				+ "'java.lang.Iterable.forEach(Consumer<? super Ani> action)' feature"
				+ "\n-------------------------------"
				+ "</html>");
		anlist.forEach((Moveable a) -> a.move());
		anlist.forEach((Moveable p) -> p.run());
		/*
		 * Traditional 'for each' call
		 */
		System.out.println("\n\nTraditional 'for each' call"
				+ "\n--------------------------------");
		for (Ani ani : anlist) {
			ani.move();
		}
//		java.util.List<E>
//		java.lang.Iterable.forEach(Consumer<? super Ani> action)
		/*
		 * Iterate java.utli.List<E> with java.lang.Iterable<T>
		 */
		System.out.println("Iterate java.util.List<E> with java.lang.Iterable<T>");
		Iterator<Ani> iterator = anlist.iterator();
		while(iterator.hasNext()) {
			Ani itAni = iterator.next();
			System.out.println("tag - " + itAni.tag + " name - " + itAni.name);
		}
	}
}

//@FunctionalInterface
interface Moveable {
	int tag = 0;
	String name = "def";
	public void move();
	public void run();
}

class Ani implements Moveable {
	int tag;
	String name;
	public Ani(int tag, String name) {
		this.tag = tag;
		this.name = name;
	}
	@Override
	public void move() {
		System.out.println(tag + ", " + name + " - \t\trunning");
	}
	@Override
	public void run() {
		System.out.println("running again");
	}
	
}