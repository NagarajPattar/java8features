package com.myzee.java8.streams;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class FindMaxInListUseMathMax {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> li = Arrays.asList(1,4,6,3,44,6,333,7,88);
		/*
		 * Method reference to static method � Class::staticMethodName
		 * Math::max
		 */
		Optional<Integer> max = li.stream().reduce(Math::max);
		li.stream().reduce(Math::max);
		System.out.println(max);
		max.ifPresent((value) -> System.out.println(value));
		
		/*
		 * Method reference to instance method from instance � ClassInstance::instanceMethodName
		 */
		max.ifPresent(System.out::println);
	}

}
