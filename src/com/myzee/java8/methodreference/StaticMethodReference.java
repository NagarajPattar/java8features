package com.myzee.java8.methodreference;
/*
 * Reference : https://youtu.be/S5zVwjtMxoc
 * Durga soft
 */
public class StaticMethodReference {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Using lambda expression");
		Say s = () -> Calling.call();	//using lambda expression
		s.saySomething();
		
		//or
		System.out.println("Using static method reference instead of lambda");
		Say s1 = Calling::call;		//using static method reference instead of lambda
		s1.saySomething();
		
	}

}

class Calling{
	
//	private static void call() {		// it works fine too
//	public static int call() {		// return any value inside method and works fine too.
	public static void call() {
		System.out.println("saying something in call ");
//		return 10;
	}
}

@FunctionalInterface
interface Say {
	public void saySomething();
}