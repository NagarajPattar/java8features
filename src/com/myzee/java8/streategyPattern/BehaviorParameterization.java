package com.myzee.java8.streategyPattern;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

public class BehaviorParameterization {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Apple> apples = Arrays.asList(
				new Apple("green", 10),
				new Apple("green", 15),
				new Apple("green", 20),
				new Apple("green", 25),
				new Apple("red", 10),
				new Apple("red", 15),
				new Apple("red", 20),
				new Apple("red", 25)
				);
		Predicate<Apple> p = t -> "green".equals(t.getName());
		List<Apple> greenAppels = filtering(apples, p);
		greenAppels.forEach(System.out::println);
		
		
		System.out.println("--------------------\n\n");
		List<Integer> numbers = Arrays.asList(1,2,3,4,5,6,7,8,9,0);
		Predicate<Integer> np = t -> t%2 == 0;
		List<Integer> evenNums = filtering(numbers, np);
		evenNums.forEach(System.out::println);
		
	}
	
	public static <T> List<T> filtering(List<T> list, Predicate<T> p) {
		List<T> result = new ArrayList<T>();
		for(T e: list) {
			if(p.test(e)) {
				result.add(e);
			}
		}
		return result;
	}

}

@AllArgsConstructor
@ToString
class Apple {
	@Getter
	private String name;
	@Getter
	private int weight;
}