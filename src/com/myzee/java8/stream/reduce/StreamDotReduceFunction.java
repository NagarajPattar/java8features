package com.myzee.java8.stream.reduce;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class StreamDotReduceFunction {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> ln = Arrays.asList(4,3,5,6,1,8);
//		Optional<Integer> res = ln.stream().reduce(StreamDotReduceFunction::total);
//		res.ifPresent(System.out::println);
		
		// OR
		
		Optional<Integer> res1 = ln.stream().reduce((a, b) -> a+b);
		res1.ifPresent(System.out::println);
		
//		System.out.println(total(3,4));
	}

	public int total(int a, int b) {
		System.out.println("call to total function");
		return a+b;
	}
}
