package com.myzee.java8.stream.map;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class HashMapUsingStreamMap {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Map<Integer, String> m = new HashMap<>();
		m.put(1, "one");
		m.put(3, "three");
		m.put(0, "zero");
		m.put(77, "sevenseven");
		m.put(45, "fourfive");

		m.entrySet().stream().forEach(e -> {
			System.out.println(e.getKey());
			System.out.println(e.getValue());
		});

		// m.keySet().stream().forEach(k -> System.out.println(m.get(k)));
		// List<Entry<Integer, String>> l = m.entrySet().stream().filter(e ->
		// e.getKey()==1 || e.getKey()==2).collect(Collectors.toList());
		Map<Integer, String> l = m.entrySet().stream().filter(e -> e.getKey() == 0 || e.getKey() == 3)
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		System.out.println("-----------------");
		System.out.println(l);

		System.out.println("sorting map using stream.sorted()");
		Map<Integer, String> msort = m.entrySet().stream().sorted(Map.Entry.comparingByKey())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		System.out.println(msort);

		System.out.println("testing\n\n");
		Map<String, Integer> unsortMap = new HashMap<>();
		unsortMap.put("z", 10);
		unsortMap.put("b", 5);
		unsortMap.put("a", 6);
		unsortMap.put("c", 20);
		unsortMap.put("d", 1);
		unsortMap.put("e", 7);
		unsortMap.put("y", 8);
		unsortMap.put("n", 99);
		unsortMap.put("g", 50);
		unsortMap.put("m", 2);
		unsortMap.put("f", 9);

		System.out.println("Original...");
		System.out.println(unsortMap);

		// sort by keys, a,b,c..., and return a new LinkedHashMap
		// toMap() will returns HashMap by default, we need LinkedHashMap to
		// keep the order.
		Map<String, Integer> result = unsortMap.entrySet().stream().sorted(Map.Entry.comparingByKey())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue,
						LinkedHashMap::new));

		// Not Recommend, but it works.
		// Alternative way to sort a Map by keys, and put it into the "result"
		// map
		Map<String, Integer> result2 = new LinkedHashMap<>();
		unsortMap.entrySet().stream().sorted(Map.Entry.comparingByKey())
				.forEachOrdered(x -> result2.put(x.getKey(), x.getValue()));

		System.out.println("Sorted...");
		System.out.println(result);
		System.out.println(result2);

		Map<Integer, String> sortMap = new TreeMap<Integer, String>(m);
		System.out.println("hurrrayyyy-------\n\n");
		System.out.println(sortMap);

		
		Map<String, Integer> testMap = new HashMap<>();
		unsortMap.put("z", 10);
		unsortMap.put("b", 5);
		unsortMap.put("a", 6);
		unsortMap.put("c", 20);
		unsortMap.put("d", 1);
		unsortMap.put("e", 7);
		unsortMap.put("y", 8);
		unsortMap.put("n", 99);
		unsortMap.put("g", 50);
		unsortMap.put("m", 2);
		unsortMap.put("f", 9);
		
		Map<String, Integer> sortit = new TreeMap<>(new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				// TODO Auto-generated method stub
				return o1.compareTo(o2);
			}
		});
		System.out.println("agaiiiinnnnn");
		sortit.putAll(unsortMap);
		System.out.println(sortit);

	}
}
