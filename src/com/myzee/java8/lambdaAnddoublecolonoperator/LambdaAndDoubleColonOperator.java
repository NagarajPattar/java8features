package com.myzee.java8.lambdaAnddoublecolonoperator;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class LambdaAndDoubleColonOperator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> nums = Arrays.asList("how", "to", "do", "in", "java", "dot", "com");
		List<String> sortNums = nums.stream().sorted((s1, s2) -> s1.compareTo(s2)).collect(Collectors.toList());
		System.out.println(sortNums);
		
		/*
		 * Using :: operator for sorting
		 * In this example, s1.compareTo(s2) is referred as String::compareTo
		 */
		System.out.println("\nUsing String::compareTo() method\n----------------------------");
		List<String> sortedNums = nums.stream().sorted(String::compareTo).collect(Collectors.toList());
		System.out.println(sortedNums);
//		nums.stream().forEach(str -> System.out::println);
		System.out.println("then again try");
		nums.stream().forEach((str -> System.out.println(str)));
	}
}
 