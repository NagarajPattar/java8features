package com.myzee.lambdatest;

@FunctionalInterface
interface Interface1 {
	public void print();
}

interface Interface2 {
	public int add(int a, int b);
}

public class FuncInterfaceUsingLambda {
	public static void main(String[] arg) {
		// Using lambda expression
		Interface1 i = () -> System.out.println("say hello to lambda");
		i.print();
		
		Interface2 i2 = (int a, int b) -> {return a+b;};
		int sum = i2.add(10,  20);
		System.out.println("sum is = " + sum);
	}
}
