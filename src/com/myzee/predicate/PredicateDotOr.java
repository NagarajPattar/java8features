package com.myzee.predicate;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class PredicateDotOr {

	public static void main(String[] args) {
		
		/*
		 * string length is less than 3 
		 * or 
		 * string starts with 'a' letter.
		 */
		// TODO Auto-generated method stub
		List<String> l = Arrays.asList("a","awt","africa","boss","dot","miss","abc","get");
		l.stream().filter(str -> str.length() <=3 || str.startsWith("a")).forEach(str -> System.out.print(str + ", "));
		System.out.println();
		
		// Using Predicate.Or()
		Predicate<String> lessThan3 = str -> str.length() <= 3;
		Predicate<String> startWithA = str -> str.startsWith("a");
		
		l.stream().filter(lessThan3.or(startWithA)).forEach(i -> System.out.print(i + ", "));;
		
		System.out.println("\n\n// Using Predicate.Or()");
		// Using Predicate.Or()
		
		List<String> li = l.stream().filter(lessThan3.or(startWithA)).collect(Collectors.toList());
		li.forEach(x -> System.out.print(x + ", "));
	}

}
